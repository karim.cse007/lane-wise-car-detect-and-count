# Import the required libraries 
import cv2 
import numpy as np 
import matplotlib.pyplot as plt 
def canny_edge_detector(image): 
	
	# Convert the image color to grayscale 
	gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY) 
	
	# Reduce noise from the image 
	blur = cv2.GaussianBlur(gray_image, (5, 5), 0) 
	canny = cv2.Canny(blur, 50, 120) 
	return canny 
def region_of_interest(image): 
	height = image.shape[0]
	width = image.shape[1]
	#print(image.shape) 
	polygons = np.array([ 
		[(0, (height-250)),(0, height),(width, height), (550, 300)] 
        ]) 
	mask = np.zeros_like(image) 
	
	# Fill poly-function deals with multiple polygon 
	cv2.fillPoly(mask, polygons, 255)
	
	# Bitwise operation between canny image and mask image 
	masked_image = cv2.bitwise_and(image, mask) 
	return masked_image 
def create_coordinates(image, line_parameters): 
	slope, intercept = line_parameters 
	y1 = image.shape[0] 
	y2 = int(y1 * (3 / 5)) 
	x1 = int((y1 - intercept) / slope) 
	x2 = int((y2 - intercept) / slope) 
	return np.array([x1, y1, x2, y2]) 
def average_slope_intercept(image, lines): 
	left_fit = [] 
	middle_fit = []
	right_fit = [] 
	for line in lines:
		#print(line)
		x1, y1, x2, y2 = line.reshape(4) 
		
		# It will fit the polynomial and the intercept and slope 
		parameters = np.polyfit((x1, x2), (y1, y2), 1) 
		slope = parameters[0]
		print(slope)
		intercept = parameters[1] 
		print(intercept)
		if slope >0:
			right_fit.append((slope, intercept))
		if slope <= -1: 
			left_fit.append((slope, intercept))
		elif slope < 0: 
			middle_fit.append((slope, intercept))
			
	left_fit_average = np.average(left_fit, axis = 0)
	middle_fit_average = np.average(middle_fit, axis = 0)
	right_fit_average = np.average(right_fit, axis = 0) 
	left_line = create_coordinates(image, left_fit_average)
	middle_line = create_coordinates(image, middle_fit_average) 
	right_line = create_coordinates(image, right_fit_average) 
	return np.array([left_line,middle_line,right_line]) 
def display_lines(image, lines): 
	line_image = np.zeros_like(image) 
	if lines is not None: 
		for x1, y1, x2, y2 in lines: 
			cv2.line(line_image, (x1, y1), (x2, y2), (255, 0, 0), 10) 
	return line_image 
# Path of dataset directory 
cap = cv2.VideoCapture("test2.mp4") 
while(cap.isOpened()): 
	_, frame = cap.read() 
	canny_image = canny_edge_detector(frame) 
	cropped_image = region_of_interest(canny_image) 
	lines = cv2.HoughLinesP(cropped_image, 4, np.pi / 180, 100, 
							np.array([]), minLineLength = 100, 
							maxLineGap = 50)
	averaged_lines = average_slope_intercept(frame, lines)
	cv2.putText(frame, "Lane 1 ", (20, 600), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255),3)
	cv2.putText(frame, "Lane 2 ", (600, 600), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255),3)
	line_image = display_lines(frame, averaged_lines)
	combo_image = cv2.addWeighted(frame, 0.8, line_image, 1, 1)
	cv2.imshow("results", combo_image) 
	
	if cv2.waitKey(1) & 0xFF == ord('q'):	 
		break

# close the video file 
cap.release() 

# destroy all the windows that is currently on 
cv2.destroyAllWindows() 

